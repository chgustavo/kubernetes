# Kubernetes

Kubernetes for the people!

## Guia de actividades para realizar
1. Instalar Minikube pagina official: https://minikube.sigs.k8s.io/docs/start/
2. Contruya una aplicacion y realice un push en registry en docker hub. Puede utilizar la que esta en el directorio *applications*.
3. Desplegar aplicacion en kubernetes, utilice variables de entorno, es decir tendra que utiliza los siguientes recursos de kubernetes:

    * deployment
    * configmap o secrets
    * service
    * ingress 

    Primero utilice manifiestos declarativos .yaml y luego utilice helm para poder templetizar la aplicacion.

4. La varibles de entorno a definir en el servicio son:
    
    * SIMPLE_SERVICE_VERSION: 1.0
    * SECRET_KEY: asdutyhgksksryfbcjssiswuey!!
    * PASSWORD_DB: MTAMYG
    * USER: cloudshine
    * PORT: 1336 

5. Agregue una nueva aplicacion (deployment y service) y exponga ese nuevo servicio en ingress.       Defina un host en ingress y un path para cada servicio.

6. Exponer utilizando ngrok o algun otro servicio. https://ngrok.com/ (opcional) 